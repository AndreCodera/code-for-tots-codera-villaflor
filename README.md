**Court Counter**

A child-friendly and colorful app that lets you keep track of the score of two basketball teams in a game.
---

## Test Cases

Scenarios, Expected Outcomes, and Results

1. Test Case #1 - click "You got 3 points!" to either teams   
	Expected Outcome - scores of both teams changes to 3  
	Test Result:  
	![alt_text](img/threep.PNG)
2. Test Case #2 - click "You got 2 points!" to either teams  
	Expected Outcome - scores of both teams changes to 5  
	Test Result:  
	![alt_text](img/twop.PNG)
3. Test Case #3 - click "You shot a freethrow!" to either teams  
	Expected Outcome - scores of both teams changes to 6  
	Test Result:  
	![alt_text](img/onep.PNG)
4. Test Case #4 - click "Back to zero!" to either teams  
	Expected Outcome - scores of both teams changes to 0  
	Test Result:  
	![alt_text](img/reset.PNG)

---

## Authors

Jan Andre V. Codera  
Renzo Vincenzi Villaflor
