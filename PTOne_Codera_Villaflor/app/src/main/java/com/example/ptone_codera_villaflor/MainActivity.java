package com.example.ptone_codera_villaflor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int teamaScore = 0;
    private int teambScore = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pt1);
    }

    public void displayScoreA (int n){
        TextView scoreA = (TextView)findViewById(R.id.teamascore);
        scoreA.setText("" + n);
    }

    public void displayScoreB (int n){
        TextView scoreB = (TextView)findViewById(R.id.teambScore);
        scoreB.setText("" + n);
    }

    public void addThreePA (View view){
        teamaScore += 3;
        displayScoreA(teamaScore);
    }

    public void addTwoPA (View view){
        teamaScore += 2;
        displayScoreA(teamaScore);
    }

    public void addOnePA (View view){
        teamaScore += 1;
        displayScoreA(teamaScore);
    }

    public void addThreePB (View view){
        teambScore += 3;
        displayScoreB(teambScore);
    }

    public void addTwoPB (View view){
        teambScore += 2;
        displayScoreB(teambScore);
    }

    public void addOnePB (View view){
        teambScore += 1;
        displayScoreB(teambScore);
    }

    public void resetVal (View view){
        teamaScore *= 0;
        teambScore *= 0;
        displayScoreA(teamaScore);
        displayScoreB(teambScore);
    }
}
